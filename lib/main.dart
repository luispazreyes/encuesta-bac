import 'package:flutter/material.dart';

import 'package:encuesta/src/bloc/provider.dart';

import 'package:encuesta/src/pages/home_page.dart';
import 'package:encuesta/src/pages/login_page.dart';
import 'package:encuesta/src/pages/encuesta_page.dart';
import 'package:encuesta/src/pages/registro_page.dart';
import 'package:encuesta/src/preferencias_usuario/preferencias_usuario.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final prefs = new PreferenciasUsuario();
  await prefs.initPrefs();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final prefs = new PreferenciasUsuario();
    print(prefs.token);

    return Provider(
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Material App',
        initialRoute: 'home',
        routes: {
          'login': (BuildContext context) => LoginPage(),
          'registro': (BuildContext context) => RegistroPage(),
          'home': (BuildContext context) => HomePage(),
          'encuesta': (BuildContext context) => EncuestaPage(),
        },
        theme: ThemeData(
          primaryColor: Color.fromRGBO(200, 16, 47, 1.0),
        ),
      ),
    );
  }
}
