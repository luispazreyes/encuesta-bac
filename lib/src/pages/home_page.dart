import 'package:flutter/material.dart';
import 'package:encuesta/src/bloc/provider.dart';
import 'package:encuesta/src/models/encuesta_model.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Home')),
      body: Center(
        child: Container(
            padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
            child: Image(
              image: AssetImage('assets/bacredomatic_logo.png'),
            )),
      ),
      floatingActionButton: _crearBoton(context),
    );
  }

  // Widget _crearListado(ProductosBloc productosBloc) {
  //   return StreamBuilder(
  //     stream: productosBloc.productosStream,
  //     builder:
  //         (BuildContext context, AsyncSnapshot<List<ProductoModel>> snapshot) {
  //       if (snapshot.hasData) {
  //         final productos = snapshot.data;

  //         return ListView.builder(
  //           itemCount: productos.length,
  //           itemBuilder: (context, i) =>
  //               _crearItem(context, productosBloc, productos[i]),
  //         );
  //       } else {
  //         return Center(child: CircularProgressIndicator());
  //       }
  //     },
  //   );
  // }

  // Widget _crearItem(BuildContext context, ProductosBloc productosBloc,
  //     ProductoModel producto) {
  //   return Dismissible(
  //       key: UniqueKey(),
  //       background: Container(
  //         color: Colors.red,
  //       ),
  //       onDismissed: (direccion) => productosBloc.borrarProducto(producto.id),
  //       child: Card(
  //         child: Column(
  //           children: <Widget>[
  //             (producto.fotoUrl == null)
  //                 ? Image(image: AssetImage('assets/no-image.png'))
  //                 : FadeInImage(
  //                     image: NetworkImage(producto.fotoUrl),
  //                     placeholder: AssetImage('assets/jar-loading.gif'),
  //                     height: 300.0,
  //                     width: double.infinity,
  //                     fit: BoxFit.cover,
  //                   ),
  //             ListTile(
  //               title: Text('${producto.titulo} - ${producto.valor}'),
  //               subtitle: Text(producto.id),
  //               onTap: () => Navigator.pushNamed(context, 'producto',
  //                   arguments: producto),
  //             ),
  //           ],
  //         ),
  //       ));
  // }

  _crearBoton(BuildContext context) {
    return FloatingActionButton(
        child: Icon(Icons.add),
        backgroundColor: Color.fromRGBO(200, 16, 47, 1.0),
        onPressed: () => Navigator.pushNamed(context, 'encuesta'));
  }
}
