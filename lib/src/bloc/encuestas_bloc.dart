import 'dart:io';

import 'package:encuesta/src/providers/encuestas_provider.dart';
import 'package:rxdart/rxdart.dart';

import 'package:encuesta/src/models/encuesta_model.dart';

class EncuestasBloc {
  final _encuestasController = new BehaviorSubject<List<EncuestaModel>>();
  final _cargandoController = new BehaviorSubject<bool>();

  final _encuestasProvider = new EncuestaProvider();

  Stream<List<EncuestaModel>> get encuestasStream =>
      _encuestasController.stream;
  Stream<bool> get cargando => _cargandoController.stream;

  // void cargarencuestas() async {
  //   final encuestas = await _encuestasProvider.cargarEncuestas();
  //   _encuestasController.sink.add(encuestas);
  // }

  void agregarEncuesta(EncuestaModel encuesta) async {
    _cargandoController.sink.add(true);
    await _encuestasProvider.crearEncuesta(encuesta);
    _cargandoController.sink.add(false);
  }

  // Future<String> subirFoto(File foto) async {
  //   _cargandoController.sink.add(true);
  //   final fotoUrl = await _encuestasProvider.subirImagen(foto);
  //   _cargandoController.sink.add(false);

  //   return fotoUrl;
  // }

  // void editarProducto(EncuestaModel producto) async {
  //   _cargandoController.sink.add(true);
  //   await _encuestasProvider.editarProducto(producto);
  //   _cargandoController.sink.add(false);
  // }

  // void borrarProducto(String id) async {
  //   await _encuestasProvider.borrarProducto(id);
  // }

  dispose() {
    _encuestasController?.close();
    _cargandoController?.close();
  }
}
